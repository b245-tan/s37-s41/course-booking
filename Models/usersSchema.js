const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Your first name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Your last name is required!"]
	},
	email: {
		type: String,
		required: [true, "Your email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required!"]
	},
	enrollments: [
			{
				courseId: {
					type: String,
					required: [true, "CourseId is required!"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Enrolled",
					required: [true, "Required!"]
				}
			}
		]
})


module.exports = mongoose.model("User", usersSchema);