const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js")


// ROUTES

// This route is responsible for the registration of the user
router.post("/register", userController.userRegistration);

// This route is for the user authentication
router.post("/login", userController.userAuthentication);

// This route is for the getProfile
router.get("/details", auth.verify, userController.getProfile);

// This route is for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse)

module.exports = router;