const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const courseController = require("../Controllers/courseController.js")

// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses);

// Route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses);

// Route for retrieving all active courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses);



// [Route with Params]
// Route for retrieving detail/s of specific course
router.get("/:courseId", courseController.courseDetails);

// 
router.put("/update/:courseId", auth.verify, courseController.updateCourse);

// [ACTIVITY]--------------------------------------------------------------------------------------
// Router for ARCHIVING a specific course
router.patch("/archive/:courseId", auth.verify, courseController.archiveCourse);


module.exports = router;