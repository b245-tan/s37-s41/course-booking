// [bcrypt]
	// npm install bcrypt

	// In our application, we will be using this package to demonstrate how to encrypt data when a user registers to our website.

	// The 'bcrypt' package is one of the many packages that we can use to encrypt information but it is not commonly recommended because of how simple the algorithm is.

	// There are other more advanced encryption packages that can be used.

	/*
		Syntax:
			bcrypt.hashSync(password, saltRounds)

			- saltRounds is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password.
	*/


// [JWT - jsonwebtoken]

	// JSON web tokens are an industry standard for sending information between our applications in a secure manner
	// The jsonwebtoken package will allow us to gain access to methods that will help us create JSON web token 