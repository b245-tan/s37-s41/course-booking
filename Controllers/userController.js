const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js")


// CONTROLLERS

// This controller will create or register a user on our db/database

module.exports.userRegistration = (request, response) => {
	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result !== null) {
			return response.send('The email is already taken!')
		} else {
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			// save to database
			newUser.save()
			.then(save => {
				return response.send("You are now registered in our website")
			})
			.catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error =>{
		return response.send(error)
	})
}


// User Authentication
module.exports.userAuthentication = (request, response) => {
	let input = request.body;

	/*
	Possible scenarios when loggin in
		1. email is not yet registered.
		2. email is registered but the password is wrong
	*/

	User.findOne({email: input.email})
	.then(result => {
		if(result == null) {
			return response.send("Email is not yet registered. Register first before loggin in!")
		} else {
			// The ".compareSync" method is used to compare a non encrypted password to the encrpted password.
			// it returns a boolean value, if match true value.
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

			if(isPasswordCorrect == true) {
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send("Password is incorrect!")
			}
		}
	})
	.catch(error => {
		return response.send(error);
	})

}


// Controller that will accept the user's id and displays the details of the user
module.exports.getProfile = (request, response) => {
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization)

	console.log(userData);

	return User.findById(userData._id).then(result => {
		result.password = '';
		return response.send(result);
	})
}


// Controller for User Enrollment:
	/*
		1. We can get the id of the user by decoding the jwt
		2. We can get the courseId by using the request params
	*/

module.exports.enrollCourse = async (request, response) => {
	// First we have to get the userId and the courseId

	// decode the token to extract/upack the payload
	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	/*
		2 Things that we need to do in this controller
			1. Push the courseId in the enrollments property of the user.
			2. Push the userId in the enrollees property of the course.
	*/

	let isUserUpdated = await User.findById(userData._id)
	.then(result => {
		if(result === null){
			return false;
		} else {

			if(Course.findById(courseId) == null){
				return response.send("Incorrect courseId!");
			} else {

				if(userData.isAdmin == true){
					return response.send("Admins are not allowed to enroll!");
				} else {
				result.enrollments.push({courseId: courseId})
				
				return result.save()
				.then(save => true)
				.catch(error => false)
				}
			}
			
		}
	})

	let isCourseUpdated = await Course.findById(courseId)
	.then(result => {
		if(result === null){
			return false;
		} else {
			result.enrollees.push({userId: userData._id})

			return result.save()
			.then(save => true)
			.catch(error => false);
		}
	})

	console.log(isCourseUpdated);
	console.log(isUserUpdated);

	if(isCourseUpdated && isUserUpdated) {
		return response.send("The course is now enrolled!");
	} else {
		return response.send("There was an error during the enrollment. Please try again!");
	}

}