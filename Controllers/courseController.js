const Course = require("../Models/coursesSchema.js")
const User = require("../Models/usersSchema.js");
const auth = require("../auth.js")

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/


module.exports.addCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	let input = request.body;

	let newCourse = new Course({
		name: input.name,
		description: input.description,
		price: input.price
	})	

	if(userData.isAdmin == false){
			response.send("You are not allowed to add course.");
		} else if(userData.isAdmin == true){
			return newCourse.save()
			// course successfully created
			.then(course =>{
				console.log(course);
				response.send("Successfully Added Course!");
			})
			// course creation failed
			.catch(error => {
				console.log(error);
				response.send(`Course creation failed`);
			})
		}

}



// Create a controller wherein it will retrieve all the courses(active/inactive courses)

module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	if(!userData.isAdmin){
		return response.send("You do not have access to this route!");
	} else {
		Course.find({})
			.then(result => response.send(result))
			.catch(error => response.send(error))
	}
}



// Create controller wherein it will retrieve only courses that are active.

module.exports.allActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

/*
	Mini Activity:
		1. You are going to create a route wherein it can retrieve all inactive courses.
		2. Make sure that the admin users only are the ones that can access this route.
*/

module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	if(!userData.isAdmin){
		return response.send("You do not have access to this route!");
	} else {
		Course.find({isActive: false})
			.then(result => response.send(result))
			.catch(error => response.send(error))
	}
}



// This controller will get the details of specific course
module.exports.courseDetails = (request, response) => {
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}


// This controller is for updating specific course
/*
	Business logic:
		1. We are going to edit/update the course that is stored in the params
*/
module.exports.updateCourse = async(request, response) => {
	const userData = auth.decode(request.headers.authorization)

	const courseId = request.params.courseId;

	const input = request.body;

	if(userData.isAdmin == false){
			response.send("You do not have access to this page!");

	} else {
		await Course.findOne({_id: courseId})
		.then(result => {
			if(result === null) {
				return response.send("CourseId is invalid, please try again!")
			}
			else {
				let updatedCourse = {
					name: input.name,
					description: input.description,
					price: input.price
				}

				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					console.log(result)
					return response.send(result)})
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
}



// [ACTIVITY]------------------------------------------------------------------------------------------------------------------------

// Controller that will ARCHIVE a specific course
module.exports.archiveCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	const courseId = request.params.courseId;	

	const input = request.body;

	if(userData.isAdmin == false){
			response.send("You do not have access to this page!");

	} else if(userData.isAdmin == true){

		Course.findOne({_id: courseId})
		.then(result => {
			if(result === null) {
				return response.send("CourseId is invalid, please try again!")
			}
			else {
				let updatedTag = {
					isActive: input.isActive
				}

				Course.findByIdAndUpdate(courseId, updatedTag, {new: true})
				.then(result => {
					console.log(result)
					return response.send(result)})
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
}